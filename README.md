This project aims to develop a web app where users can view their instagram account and text analysis performed on their post's comments. For this, I have used [Instagram Private API](https://github.com/ping/instagram_private_api)

Currently:
1. Dashboard display similar to instagram.
2. Post Modal shows the post content and comments.
3. Color of comment box shows the nature of the comment. 
    - **Red** --> Hate comment
    - **Yellow** --> Sarcastic comment
    - **Blue** --> Normal comment
4. Only images and videos in the post is supported.
5. Carousel posts are not displayed properly.

First, install the requirements. It is recommended to use a virtual environment
```
pip install -r requirements.txt
```

` Data/dataExtractor.py ` retrieves all posts and comments into seperate folder according to username. Authenticate the file with your instagram account credentials. For now, I have limited the retrieval of comments to 15 posts. It takes a while to retrieve all comments of each post, it can be modified inside the file. 

Follow this to get the app working
```
cd Data
python dataExtractor.py
cd ..
python main.py
```


